<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version details
 *
 * @package    block_bcu_course_checks
 * @copyright  2014 Michael Grant <michael.grant@bcu.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */

// Strings for settings form.

$string['assignmentcheckdonotshow'] = 'Do not show Assignments';
$string['assignmentcheckopt'] = 'Check if assignments are due before the course starts?';
$string['assignmentcheckpastassignments'] = 'Check if assignments are due before the course starts';
$string['assignmentcheckshowall'] = 'Show All Assignments and their due dates';
$string['assignmentdates'] = 'Assignment Dates';
$string['assignmentlistcheck'] = 'Assignment check';
$string['assignmentlistcheckdesc'] = 'Assignment checklist for checking assignment due dates or past assignments';
$string['bcu_course_checks:addinstance'] = 'Add a new course check block';
$string['blocksenabledopt'] = 'What blocks would you encourage on courses?';
$string['blockshelplinkopt'] = 'How to Edit Blocks...'; //
$string['blocktitle']='Course Quicksets'; //
$string['cachedef_coursechecksdata'] = 'Course checks caching';
$string['changesettingsheader']='Quick Settings';
$string['cleanpermfailure'] = 'Sorry, there was an error when trying to clean this course.';
$string['cleansuccess'] = 'Your course has been cleaned up, {$a} sections have been removed.';
$string['cleanupbutton'] = 'Delete Empty Sections!';
$string['contentsections'] = 'Content';
$string['coursecleanupopt'] = 'Allow users to clean course?';
$string['courseguestopt'] = 'Check that guest access is disabled for the course';
$string['courseguidecheck'] = 'Check course guides';
$string['courseguidecheckdesc'] = 'Check course guides for completion';
$string['courseguidechecktitle'] = 'Check course guides';
$string['courseimage'] = 'Set a course image?';
$string['courseimageopt'] = 'Check that course image is set?';
$string['courserenamesecopt'] = 'Should all sections be renamed?';
$string['coursesummary'] = 'Set course summary?';
$string['coursesummaryopt'] = 'Check that course summary isn\'t empty?';
$string['courseuseropt'] = 'Allow the user to set their own course checks?';
$string['coursevisible'] = 'This course is hidden.';
$string['coursevisibleopt'] = 'Check if the course is visible to students?';
$string['creationprogress'] = 'Quicksets...';
$string['dates'] = 'Dates';
$string['editalldateslink']='Edit all Course Dates...';
$string['editalllink']='Edit all settings...';
$string['entryform']='Make Changes Here';
$string['essentials'] = 'Style';
$string['externallinkdesc'] = 'External Link'; //
$string['externallinkopt'] = 'External Link'; //
$string['externallinktext'] = 'Text for External Link'; //
$string['externallinktextdesc'] = 'Text for External Link'; //
$string['formatsnameopt'] = 'Which formats do you want to prompt the sections to be renamed on?';
$string['guestaccess'] = 'Is guest access disabled?';
$string['helplinkopt'] = 'Help (Opens in new window)';
$string['newcourseimage'] = 'Add course image';
$string['nostudents']='not visible to students!';
$string['pluginname'] = 'Quickset';
$string['renamedsections'] = 'Rename';
$string['sectionchecks'] = 'Section checks';
$string['sectioncontentopt'] = 'Should all sections have content?';
$string['sectionnameunnamed'] = 'Unnamed';
$string['sectionshelplinkopt'] = 'How to Add Sections...';
$string['sectionsummaryopt'] = 'Should all sections have a summary?';
$string['sectionvisibleopt'] = 'Should all sections be visible?';
$string['settingslite'] = 'View Basics'; //
$string['suggestedblocks'] = 'Suggested blocks'; 
$string['summarysections'] = 'Summary';
$string['toggle_coursevis']='Course visible'; //
$string['updatecourseimage'] = 'Update course image';
$string['updatequicksets']='Update these settings';
$string['visibilitybutton'] = 'Course Visibility Toggle'; //
$string['visiblesections'] = 'Visible'; 
$string['withselected']='WITH SELECTED';
