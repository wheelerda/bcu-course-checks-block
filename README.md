CLAMP Quicksets Block
from the BCU Course Checks Block
============================
This block carries out a number of automated checks on a course, that are all user configurable. The checks were designed to encourage a minimum standard of course quality, instead of forcing users and restricting their abilities.

The block also has the option to allow users to "clear down" a course, simply removing any sections that don't have content. It won't delete any sections that have content and/or a summary.

The checks that can be carried out by this block are:

* Has the course got a summary, and image set?
* Is the course visible to students?
* Has guest access been disabled on the course (helps prevent issues with automatically enrolled students)
* Have the sections been renamed from their default heading?
* Have all of the sections got a summary?
* Have all sections got content
* What blocks should each course have added to them?

Colgate has used this block to implement some of the features from Bob Puffer's Quickset block, features which we consider
essential for our U/I. These features are primarily:

* View and set course visibility
* View and set grades visibility

We have also modified original Course Checks functionality, including:

* Added a collapsible section to display selected course settings
* Modified Assignment dates section to display due date (and identify when a due date was not set)
* Added help links in various sections to appropriate help/descriptions or settings pages

Maintainer
----------
The block has been written and is currently maintained by Michael Grant working at Birmingham City University.

Adapters
--------
Collaborative Liberal Arts Moodle Project (CLAMP) http://clamp-it.org
Joe Burgess at Butler University
Dan Wheeler at Colgate University (DWheeler@Colgate.Edu)

Documentation
-------------
Documentation can be found at [the page at Moodle wiki](http://docs.moodle.org/en/Course_checks_block)
