<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version details
 *
 * @package    block_bcu_course_checks
 * @copyright  2014 Michael Grant <michael.grant@bcu.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */

defined('MOODLE_INTERNAL') || die;

require_once($CFG->dirroot.'/blocks/bcu_course_checks/locallib.php');
require_once($CFG->libdir.'/weblib.php');

/**
 * Course checks block renderer class.
 *
 * @package   block_bcu_course_checks
 *
 * @copyright 2014 Michael Grant <michael.grant@bcu.ac.uk>
 * @copyright 2017 Manoj Solanki (Coventry University)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class block_bcu_course_checks_renderer extends plugin_renderer_base {

    /** @var stdClass Used to store config details */
    private $_config;

    /**
     * Set up instance config based on sitewide settings.
     *
     * @param stdClass $config
     *
     */
    public function setconfig($config) {
        $this->_config = $config;
        if ($this->_config === null) {
            $this->_config = new stdClass();
        }
        $defaults = get_config('bcu_course_checks');
        foreach ($defaults as $key => $value) {
            if (get_config('bcu_course_checks', 'usersoptions') === '1') {
                if (!ISSET($this->_config->$key)) {
                    $this->_config->$key = $value;
                }
            } else {
                $this->_config->$key = $value;
            }
        }
    }

    /**
     * Render course status section.
     *
     * @param int $courseid
     *
     */
    public function render_course_status($courseid) {
        $blocksections = array();
        $content = '';
        $essentials = '';
        $recommended = '';
        $status = false;
        $courseinfo = block_bcu_course_checks_get_course_properties($courseid);

        if ($this->_config->coursevisible) {
            $content .= $this->render_visibility($this->is_property_set($courseinfo->visible));
        }

        if ($this->_config->courseimage) {
            $courseoverviewfiles = block_bcu_course_checks_get_course_overviewfiles($courseid);
            $essentials .= get_string('courseimage', 'block_bcu_course_checks') . $this->set_image($courseoverviewfiles)."<br>";
            if (!$courseoverviewfiles) {
                $status = true;
            }
        }

        if ($this->_config->coursesummary) {
            $essentials .= get_string('coursesummary', 'block_bcu_course_checks') .
                           $this->set_image($this->is_property_set($courseinfo->summary)).'<br>';
            if (!$courseinfo->summary) {
                $status = true;
            }
        }

        if ($this->_config->courseguest) {
            $courseguestaccess = block_bcu_course_checks_get_guest_access($courseid);
            $essentials .= get_string('guestaccess', 'block_bcu_course_checks') .
                           $this->set_image($this->is_property_set($courseguestaccess)).'<br>';
            if (!$courseguestaccess) {
                $status = true;
            }
        }

        if (strlen($essentials) > 0) {
            $content .= $this->render_check_section($essentials, 'block_bcu_course_checks_essentials', 'essentials',
                        $status, false);
            $blocksections[] = 'block_bcu_course_checks_essentials';
        }

        if (isset($this->_config->assignmentlistcheck) &&
                ($this->_config->assignmentlistcheck != block_bcu_course_checks::ASSIGNMENT_CHECK_DO_NOT_SHOW) ) {
            switch ($this->_config->assignmentlistcheck) {
                case block_bcu_course_checks::ASSIGNMENT_CHECK_SHOW_ALL:
                    $assignments = block_bcu_course_checks_get_assignments($courseinfo);
                    break;
                case block_bcu_course_checks::ASSIGNMENT_CHECK_PAST_ASSIGNMENTS:
                    $assignments = block_bcu_course_checks_get_outdated_assignments($courseinfo);
                    break;
            }
            $content .= $this->render_check_section($assignments['content'], 'block_bcu_course_checks_assignments',
                        'assignmentdates', $assignments['status'], false);
            $blocksections[] = 'block_bcu_course_checks_assignments';
        }

        $sections = block_bcu_course_checks_check_sections($courseid);

        if ($sections && ($this->_config->renamedsections || $this->_config->summarysections
                || $this->_config->contentsections || $this->_config->visiblesections)) {

            $courseformat = block_bcu_course_checks_check_format($courseid);
            $recommended = $this->check_sections($sections, $courseformat);
            $content .= $this->render_check_section($recommended['table'], 'block_bcu_course_checks_sections',
                                                    'sectionchecks', $recommended['status'], false);
            $blocksections[] = 'block_bcu_course_checks_sections';
        }

        if (!empty($this->_config->blocksenabled)) {
            $courseblocks = block_bcu_course_checks_get_course_blocks($courseinfo);
            $blocks = '';
            $anyfails = false;
            foreach ($courseblocks as $key => $value) {
                if (!$value) {
                    $anyfails = true;
                }
                $blocks .= $key . $this->set_image($value)."<br>";
            }
            $content .= $this->render_check_section($blocks, 'block_bcu_course_checks_blocks', 'suggestedblocks', $anyfails, false);
            $blocksections[] = 'block_bcu_course_checks_blocks';

        }

        // Check any course guides for completion.
        if (!empty($this->_config->courseguidecheck)) {
            $courseguidestatuses = block_bcu_course_checks_get_courseguide_statuses($courseid);

            if (!empty($courseguidestatuses)) {
                $courseguideresult = $this->format_courseguide_results($courseguidestatuses);
                $content .= $this->render_check_section($courseguideresult['content'],
                            'block_bcu_course_checks_courseguide', 'courseguidechecktitle', $courseguideresult['status'], false);
                $blocksections[] = 'block_bcu_course_checks_courseguide';
            }

        }

        return array('content' => $content, 'sections' => $blocksections);
    }

    /**
     * Render a section.
     *
     * @param string $content
     * @param int    $id
     * @param string $title
     * @param bool   $status
     * @param bool   $expanded
     *
     */
    public function render_check_section($content, $id, $title, $status=true, $expanded=null) {
        if ($expanded === null) {
            $expanded = $status;
        }
        return print_collapsible_region($content, 'block_bcu_course_checks_collapsible',
                                        $id, $this->render_title($title, $status), '', !$expanded, true);
    }

    /**
     * Render course visibility.
     *
     * @param bool $status
     *
     */
    public function render_visibility($status) {
        if (!$status) {
            return html_writer::div(html_writer::tag('i', null,
                                    array('class' => 'fa fa-exclamation-triangle')) . get_string('coursevisible',
                                            'block_bcu_course_checks'), 'alert alert-danger', array('id' => 'visibility-alert'));
        }
    }

    /**
     * Render title for the block.
     *
     * @param string $title
     * @param bool   $status
     *
     */
    public function render_title($title, $status) {
        $content = get_string($title, 'block_bcu_course_checks');

        if ($status) {
            // We use the expanded setting to track if there are issues, and need to flip it for the correct image.
            $content .= $this->set_image(false);
        } else {
            $content .= $this->set_image(true);
        }
        return $content;
    }

    /**
     * Render clean course button.
     *
     * @param int $courseid
     *
     */
    public function render_cleanup($courseid) {
        GLOBAL $PAGE;
        $cleanup = '';
        if ($this->_config->coursecleanup && $PAGE->user_is_editing() && block_bcu_course_checks_can_cleanup($courseid) > 0) {
            $cleanup = html_writer::link(new moodle_url('', array('cleanme' => true, 'id' => $courseid, 'sesskey' => sesskey())),
                    get_string('cleanupbutton', 'block_bcu_course_checks'),
                    array('class' => 'btn btn-primary btn-large btn-block'));
        }

        return $cleanup;
    }

    /**
     * Render help link.
     */
    public function render_helplink() {
        if ($this->_config->helplink) {
            return html_writer::link($this->_config->helplink,
                                     get_string('helplinkopt', 'block_bcu_course_checks'),
                                     array('target' => '_blank', 'class' => 'coursecheckshelp'));
        }
    }

    /**
     * Render check sections.
     *
     * @param array    $sections
     * @param stdClass $courseformat
     *
     */
    public function check_sections($sections, $courseformat) {

        $tableheadings = array(
            'renamedsections' => get_string('renamedsections', 'block_bcu_course_checks'),
            'summarysections' => get_string('summarysections', 'block_bcu_course_checks'),
            'contentsections' => get_string('contentsections', 'block_bcu_course_checks'),
            'visiblesections' => get_string('visiblesections', 'block_bcu_course_checks')
        );

        $formats = explode(',', $this->_config->formatsname);
        if (!in_array($courseformat->format, $formats)) {
            $this->_config->renamedsections = false;
        }
        $table = new html_table();
        $table->data        = array();
        $table->attributes['class'] = 'generaltable bcu_course_checks sectiontable';
        $table->size = array("35%", "16%", "16%", "16%", "16%");
        $table->head        = array(' ');

        foreach ($tableheadings as $key => $value) {
            if ($this->_config->$key) {
                $table->head[] = $value;
            }
        }
        $anyfails = false;

        foreach ($sections as $key => $value) {
            list($renamed, $summary, $content, $visible) = array(true, true, true, true);
            $row = new html_table_row();

            $sectionname = '';
            if (strlen($value['name']) > 20) {
                $sectionname = substr($value['name'], 0, 22).'...';
            } else {
                $sectionname = $value['name'];
            }

            $cells[] = new html_table_cell($sectionname);

            if ($this->_config->renamedsections) {
                $renamed = $this->is_property_set($value['nameisset']);
                $cells[] = new html_table_cell($this->set_image($renamed, 'tableimg'));
            }
            if ($this->_config->summarysections) {
                $summary = $this->is_property_set($value['summary']);
                $cells[] = new html_table_cell($this->set_image($summary, 'tableimg'));
            }
            if ($this->_config->contentsections) {
                $content = $this->is_property_set($value['content']);
                $cells[] = new html_table_cell($this->set_image($content, 'tableimg'));
            }
            if ($this->_config->visiblesections) {
                $visible = $this->is_property_set($value['visible']);
                $cells[] = new html_table_cell($this->set_image($visible, 'tableimg'));
            }
            $row->cells = $cells;
            $cells = array();
            $table->data[] = $row;

            if (!$renamed || !$summary || !$content || !$visible) {
                $anyfails = true;
            }
        }
        $tabular = html_writer::table($table);

        $table = html_writer::tag('div', $tabular, array('style' => 'display: table', 'class' => 'table-responsive'));
        return array('table' => $table, 'status' => $anyfails);
    }

    /**
     * Format the the course guide data for display.
     *
     * @param array $templateresults
     *
     */
    public function format_courseguide_results($templateresults) {
        global $OUTPUT;

        $checksfailed = false;  // Actually means that section check is ok (tick).

        $content = html_writer::start_tag('fieldset', null, array('class' => 'clearfix collapsible'));

        // Go through each course guide module.
        foreach ($templateresults as $templateresult) {

            // Course guide link.
            $editcoursemodulelink = html_writer::tag('span', html_writer::link(new
                    moodle_url('/course/modedit.php', array('update' => $templateresult['cmid'], 'return' => 1)),
                    $OUTPUT->pix_icon('t/edit', '')) . ' &nbsp;', array('class' => 'bcu_course_status'));

            // Prepare strings containing status and completion date for completed activities.
            $templatecmstatus = '';
            $templatecmstext = '';
            if ($templateresult['results']->completed) {
                $templatecmstatus = $this->set_image(true);
                $templatecmstext = 'Completed on <br>' .
                                    userdate($templateresult['results']->completedtime, '%a %d %b %Y, %l:%M %p');
            } else {
                $checksfailed = true;
                $templatecmstatus = $this->set_image(false);
            }

            $templatecmstatus = html_writer::tag('span', $templatecmstatus);

            // Course guide name.
            $courseguidename = '';
            if (strlen($templateresult['cm_name']) > 30) {
                $courseguidename = substr($templateresult['cm_name'], 0, 30).'...';
            } else {
                $courseguidename = $templateresult['cm_name'];
            }

            // Course guide edit link.
            $cmlink = html_writer::link(new moodle_url($templateresult['cm_url']), $courseguidename);

            $content .= html_writer::tag('div', '<strong>' . $cmlink . '</strong>' . $templatecmstatus .
                    $editcoursemodulelink, array('class' => '', 'title' => $templateresult['cm_name']));

            // If completed, then display the completed date information that is in $templatecmstext.
            if ($templatecmstext) {
                $content .= html_writer::tag('span', $templatecmstext);
            }

            $content .= html_writer::empty_tag('hr', array('class' => 'assignmenthr'));

        }

        $content .= html_writer::end_tag('fieldset');

        return array('content' => $content, 'status' => $checksfailed);
    }

    /**
     * Render image (current an X or tick).
     *
     * @param bool  $status
     * @param string $class
     *
     */
    public function set_image($status, $class='bcu_course_status') {
        global $OUTPUT;
        if ($status) {
            return html_writer::tag('span', $OUTPUT->pix_icon('i/grade_correct', ''), array('class' => $class));
        } else {
            $this->errors = true;
            return html_writer::tag('span', $OUTPUT->pix_icon('i/grade_incorrect', ''), array('class' => $class));;
        }
    }

    /**
     * Check if variable is set to true or false.
     *
     * @param bool $courseproperty
     *
     */
    public function is_property_set($courseproperty) {
        if ($courseproperty) {
            return true;
        }
        return false;
    }
}